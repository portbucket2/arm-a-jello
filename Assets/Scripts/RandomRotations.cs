﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class RandomRotations : MonoBehaviour
{
    //public Space space = Space.World;
    public Vector3 worldAxis = new Vector3(0,0,1);
    public float minRot;
    public float maxRot;

    float rotRate;
    int key;
    private void OnEnable()
    {
        rotRate = Random.Range(minRot,maxRot);
        key = Centralizer.Add_Update(this, OnUpdate);
        SetRot(0);
    }
    private void OnDisable()
    {
        Centralizer.Remove_Update(key);
    }

    void SetRot(float angle)
    {
        this.transform.localRotation = Quaternion.identity * Quaternion.Euler(worldAxis*angle);
    }
    void OnUpdate()
    {
        this.transform.localRotation = this.transform.localRotation * Quaternion.Euler(worldAxis * rotRate*Time.deltaTime);
    }
}
