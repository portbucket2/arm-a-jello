﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class OverlapAttack : MonoBehaviour
{
    public float damagePerSec= 1000;
    public List<Collider> colliders;
    //AttackArm aarm;
    //ArmProfile Profile => aarm.profile;
    // Start is called before the first frame update
    void Start()
    {
        //aarm = GetComponent<AttackArm>();   
        foreach(Collider col in colliders)
        {
            col.isTrigger = true; 
        }

        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = false;
    }

    private void OnTriggerStay(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        //Debug.Log(agent);
        if (agent)
        {
            agent.ChangeHP(-damagePerSec * Time.fixedDeltaTime,DamageFXType.WEAK);
        }
    }
}
