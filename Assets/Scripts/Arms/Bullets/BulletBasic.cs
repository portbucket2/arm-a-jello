﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
public class BulletBasic : MonoBehaviour
{
    public float autoFuseRange;
    public float moveSpeed;

    float FuseTime => (autoFuseRange / moveSpeed)+initTime;

    protected float damage;
    bool fused;

    float initTime;
    public void Init(float damage)
    {
        this.damage = damage;
        initTime = Time.time;
        fused = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!fused)
        {
            Agent agent = other.GetComponentInParent<Agent>();
            //Debug.Log(agent);
            if (agent && agent.alive)
            {
                OnImpact(agent);
                Fuse();
            }
        }
    }
    protected virtual void OnImpact(Agent agent)
    {
        agent.ChangeHP(-damage,DamageFXType.WEAK);
    }

    void Fuse()
    {
        if (fused) return;
        fused = true;
        Centralizer.Add_DelayedAct(() =>
        {
            Pool.Destroy(this.gameObject);
        }, 0.1f);
    }

    Vector3 FaceDir => transform.right;
    private void Update()
    {
        this.transform.position = this.transform.position + FaceDir*moveSpeed*Time.deltaTime;
        if (Time.time > FuseTime) Fuse();
      
    }
}
