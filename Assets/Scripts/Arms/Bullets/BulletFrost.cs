﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
public class BulletFrost : BulletBasic
{
    protected override void OnImpact(Agent agent)
    {
        
        IStatusAffectable isa =  agent.GetComponent<IStatusAffectable>();
        if (isa != null) isa.SlowFor(damage);
    }
}
