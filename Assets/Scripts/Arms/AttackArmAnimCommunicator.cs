﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class AttackArmAnimCommunicator : MonoBehaviour
{
    public ParticleSystem ps;
    internal AttackArm armRef;
    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
        
    }
    public void AttackStart()
    {
        anim.SetTrigger("attack");//attack anmimation should trigger damage calls;
    }
    public void OnAttackPointReached()
    {
        //Debug.LogError("Attacking");
        armRef.AttackNow();
    }
    public void OnParticlePlayIfAny()
    {
        if (ps) ps.Play();
    }
}
