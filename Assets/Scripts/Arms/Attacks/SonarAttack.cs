﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

[RequireComponent(typeof(Rigidbody))]
public class SonarAttack : AttackArm
{
    [Header("Sonar Specific Attributes")]
    [Range(0, 90)]
    public float maxAngle;
    public float waveSpeed;
    public Transform spawnPoint;
    public MeshRenderer sonarRenderer;
    public List<Collider> colliders;
    List<Agent> inAgents = new List<Agent>();


    MaterialPropertyBlock mpb;
    public override void AttackNow()
    {
        CleanupAgentList();
        Vector3 waveDir = spawnPoint.transform.right;
        base.AttackNow();
        SonarSync();
        foreach (Agent a in inAgents)
        {
            Vector3 enemyVec = a.transform.position - spawnPoint.position;
            float dist = enemyVec.magnitude - a.estimatedRadius;
            float angle = Vector3.Angle(enemyVec, waveDir);

            float dRatio = 1 - Mathf.Clamp01(dist / profile.damage);
            if (angle <= maxAngle && dist <= profile.damage)
            {
                float timeToReach = enemyVec.magnitude / waveSpeed;
                Centralizer.Add_DelayedAct(() => {
                    if(a&&a.alive)
                    {
                        (a.GetComponent<EnemyMotion>() as IStatusAffectable).AddKnockBack(profile.damage * Mathf.Pow(dRatio, 0.35f));
                    }
                }, timeToReach);
            }
        }
    }
    void CleanupAgentList()
    {
        for (int i = inAgents.Count - 1; i >= 0; i--)
        {
            Agent a = inAgents[i];
            if (a && a.alive) continue;
            inAgents.RemoveAt(i);
        }
    }
    private void SonarSync()
    {
        //Debug.Log(Time.time);
        sonarRenderer.gameObject.SetActive(true);

        mpb.SetFloat("_Speed", waveSpeed);
        mpb.SetFloat("_Frequency", profile.attackCooldown);
        mpb.SetFloat("_ArcAngle", maxAngle);
        mpb.SetFloat("_ReferenceTime", Time.time);
        sonarRenderer.SetPropertyBlock(mpb);
    }

    protected override void OnInit()
    {
        base.OnInit();
        //aarm = GetComponent<AttackArm>();   
        foreach (Collider col in colliders)
        {
            col.isTrigger = true;
        }

        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = false;
        sonarRenderer.gameObject.SetActive(false);
        mpb = new MaterialPropertyBlock();
        sonarRenderer.GetPropertyBlock(mpb);
    }



    private void OnTriggerEnter(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        //Debug.Log(agent);
        if (agent)
        {
            inAgents.Add(agent);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        //Debug.Log(agent);
        if (agent && inAgents.Contains(agent))
        {
            inAgents.Remove(agent);
        }
    }


}
