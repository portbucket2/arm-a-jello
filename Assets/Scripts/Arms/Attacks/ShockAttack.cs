﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ShockAttack : AttackArm
{
    public float shockDuration = 2;
    public List<Collider> colliders;
    List<Agent> inAgents = new List<Agent>();


    public override void AttackNow()
    {
        CleanupAgentList();
        base.AttackNow();
        foreach (Agent a in inAgents)
        {
            a.GetComponent<IStatusAffectable>().StunFor(shockDuration);
            a.ChangeHP(-profile.damage,DamageFXType.NONE);
        }
    }
    void CleanupAgentList()
    {
        for (int i = inAgents.Count - 1; i >= 0; i--)
        {
            Agent a = inAgents[i];
            if (a && a.alive) continue;
            inAgents.RemoveAt(i);
        }
    }
    protected override void OnInit()
    {
        base.OnInit();        
        //aarm = GetComponent<AttackArm>();   
        foreach (Collider col in colliders)
        {
            col.isTrigger = true;
        }

        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = false;
    }



    private void OnTriggerEnter(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        //Debug.Log(agent);
        if (agent)
        {
            inAgents.Add(agent);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        //Debug.Log(agent);
        if (agent && inAgents.Contains(agent))
        {
            inAgents.Remove(agent);
        }
    }

    
}
