﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeriodicAttack : MonoBehaviour
{
    public float attackCooldown = 1;
    public bool onCooldown { get { return Time.time < lastAttackTime + attackCooldown; } }


    public float lastAttackTime;

    public virtual void InitiateAttack()
    {

    }
}
