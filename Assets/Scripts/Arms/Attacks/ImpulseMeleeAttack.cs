﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ImpulseMeleeAttack : AttackArm
{
    public List<Collider> colliders;
    List<Agent> inAgents = new List<Agent>();
    public Animator anim;
    public DamageFXType damageFX;
    public override void AttackNow()
    {
        CleanupAgentList();
        base.AttackNow();
        foreach (Agent a in inAgents)
        {
            a.ChangeHP(-profile.damage, damageFX);
        }
    }
    void CleanupAgentList()
    {
        for (int i = inAgents.Count - 1; i >= 0; i--)
        {
            Agent a = inAgents[i];
            if (a && a.alive) continue;
            inAgents.RemoveAt(i);
        }
    }
    protected override void OnInit()
    {
        base.OnInit();        
        //aarm = GetComponent<AttackArm>();   
        foreach (Collider col in colliders)
        {
            col.isTrigger = true;
        }

        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = false;
    }



    private void OnTriggerEnter(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        agent.onDeath += AnimRecheck;
        //Debug.Log(agent);
        if (agent)
        {
            inAgents.Add(agent);
        }
        AnimRecheck();
    }
    private void OnTriggerExit(Collider other)
    {
        Agent agent = other.GetComponentInParent<Agent>();
        agent.onDeath -= AnimRecheck;
        //Debug.Log(agent);
        if (agent && inAgents.Contains(agent))
        {
            inAgents.Remove(agent);
        }
        AnimRecheck();
    }

    void AnimRecheck()
    {
        if (anim)
        {
            CleanupAgentList();
            anim.SetBool("hasEnemy", inAgents.Count > 0);
        }
    }

    
}
