﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class RangeAttack : AttackArm
{
    public Transform spawnPoint;
    public GameObject prefab;
    public override void AttackNow()
    {
        base.AttackNow();
        BulletBasic bbase = Pool.Instantiate(prefab,spawnPoint.position,spawnPoint.rotation).GetComponent<BulletBasic>();
        bbase.Init(profile.damage);

    }
}
