﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class AttackArm : MonoBehaviour
{
    [SerializeField]  protected bool log;
    public Sprite profileImage;
    public ArmProfile profile;
    public AttackArmAnimCommunicator animCom;
    public PointerController pointer;
    public ArmDataKeep dataRef;
    


    LayerMask enemyLayers;

    public bool freeFire;
    public bool onCooldown { get {return Time.time < lastAttackTime + profile.attackCooldown; }}

    float lastAttackTime;
    public void Init(ArmDataKeep adk)
    {
        if (!animCom) GetComponentInChildren<AttackArmAnimCommunicator>();
        if (animCom) animCom.armRef = this;
        dataRef = adk; 
        
        enemyLayers = 1 << LayerMask.NameToLayer("Enemy");
        lastAttackTime = -100;
        OnInit();
    }


    protected virtual void OnInit()
    { 
    }

    private void Update()
    {
        if (GameStateManager.inAction && !onCooldown)
        {
            RaycastHit rch;
            if (freeFire || Physics.Raycast(transform.position, transform.up, out rch, profile.detectionRange, enemyLayers))
            {
                InitiateAttack();
            }
        }
    }

    public virtual void InitiateAttack()
    {
        lastAttackTime = Time.time;
        if (animCom) animCom.AttackStart();//attack anmimation should trigger damage calls;
        else
        {
            Centralizer.Add_DelayedAct(()=> {
                AttackNow();
            },0.25f);
        }
    }

    public virtual void AttackNow()
    {
    }
}
[System.Serializable]
public class ArmProfile
{
    public ArmType armType;
    public float damage=100;
    public float attackCooldown=1;
    public float detectionRange=5;
}
public enum ArmType
{
    Basic = 0,
    Crab = 1,
    Star =2, 
    Eel = 3,
    Sword =4,
    Sonar = 5,
    Frost = 6,

}
