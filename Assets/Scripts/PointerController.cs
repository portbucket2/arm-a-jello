﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointerController : MonoBehaviour
{
    public Animator anim;
    public Button arrowUIButton;
    public Transform root;
    public Canvas canv;
    private void Start()
    {
        canv.worldCamera = Camera.main;
        TurnOff();
    }
    public void Load(bool upgrade, float angleDegrees, System.Action onButton)
    {
        root.gameObject.SetActive(true);

        this.transform.localRotation = Quaternion.identity * Quaternion.Euler(transform.forward* angleDegrees);
        anim.SetTrigger(upgrade ? "Upgrade" : "New");
        arrowUIButton.onClick.RemoveAllListeners();
        arrowUIButton.onClick.AddListener(()=> 
        { 
            onButton?.Invoke();
            arrowUIButton.onClick.RemoveAllListeners();
        });
    }
    public void TurnOff()
    {
        root.gameObject.SetActive(false);
    }
}
