﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class JellyCollsionController : MonoBehaviour
{
    public Agent jellyAgent;

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponentInParent<Enemy>();
        Debug.Log(enemy);
        if (enemy&& enemy.selfAgent.alive)
        {
            Agent enemyAgent = enemy.GetComponent<Agent>();
            //sequence sensitive
            //kill player first otherwise you can tank the last enemy and become winner
            jellyAgent.ChangeHP(-1,DamageFXType.NONE);
            enemy.GetComponent<Agent>().ChangeHP(-1000,DamageFXType.HEAVY);
        }
    }
}
