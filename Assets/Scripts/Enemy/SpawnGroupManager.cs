﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
//#if UNITY_EDITOR
//using UnityEditor;
//using System.Linq;
//using System;
//using System.Reflection;
//#endif
public class SpawnGroupManager : MonoBehaviour
{
    public static SpawnGroupManager Instance { get; private set; }
    public static System.Action<float> levelProgressUpdated;
    public float progress { get { return deathCount / totalSpawnableCount; } }
    public float baseDistance = 13;
    public float reachDistance = 2;
    public List<SpawnGroup> spawns;

    [Header("Observe Only")]
    [SerializeField] bool spawnListEmpty;
    [SerializeField] int activeSpwnCount;
    [SerializeField] float deathCount;
    [SerializeField] int totalSpawnableCount;
    [SerializeField] Vector3 rootPos;
    void Start()
    {
        Instance = this;
        spawnListEmpty = false;
        rootPos = Player.Instance.transform.position;
        foreach (SpawnGroup s in spawns)
        {
            totalSpawnableCount += s.enemies.Count;
        }
        //Debug.LogFormat("totalSpawnableCount {0}", totalSpawnableCount);
        GameStateManager.stateAccess.AddStateEntryCallback(GameState.Action,OnActionStart);
    }

    void OnActionStart()
    {
        //Spawn(spawns[0]);
        StartCoroutine(Routine());
    }
    IEnumerator Routine()
    {
        float lastGap=0;
        for (int i = 0; i < spawns.Count; i++)
        {
            SpawnGroup spg = spawns[i];
            //Debug.LogFormat("Waiting for {0} + {1}", lastGap, spg.gapOffset );
            yield return new WaitForSeconds(lastGap*spg.gapRatio + spg.gapOffset);
            //Debug.LogFormat("Spawning Group {0}",i);
            lastGap = spg.LongestReachTime(baseDistance-reachDistance);
            Spawn(i);
        }
        spawnListEmpty = true;
    }

    public ChancedList<float> spawnAngles = new ChancedList<float>();
    // Update is called once per frame
    void Spawn(int groupIndex)
    {
        SpawnGroup spg = spawns[groupIndex];
        int spwnPointCount = Mathf.Max(PlayerDataController.ArmCount,spg.enemies.Count);
        float angleGapBase = 360.0f / spwnPointCount;

        float baseRot = Random.Range(0, 360.0f);
        spawnAngles.Clear();
        for (int i = 0; i < spwnPointCount; i++)
        {
            float rot = (baseRot + i * angleGapBase);
            if (rot > 360) rot -= 360;
            //Debug.LogFormat("Loaded {0}",rot);
            spawnAngles.Add(rot, 1.0f);
        }
        
        for (int i = 0; i < spg.enemies.Count; i++)
        {
            float angleChosen = spawnAngles.RollOneShot();
            //Debug.LogFormat("chosen {0}", angleChosen);
            Vector3 posDir = (Quaternion.Euler(0, 0, angleChosen) * Vector3.right).normalized;
            Vector3 pos = rootPos + posDir * baseDistance;

            GameObject go = Instantiate(spg.enemies[i].gameObject, pos , spg.enemies[i].transform.rotation);
            go.name = string.Format("G{0}_{1}",groupIndex, spg.enemies[i].gameObject.name);
            Enemy enemy = go.GetComponent<Enemy>();
            enemy.Init(OnEnemyDeath);

            //Debug.LogFormat("Spawning {0}", go.name);
            activeSpwnCount++;
        }
    }
    void OnEnemyDeath()
    {
        deathCount++;
        levelProgressUpdated?.Invoke(progress);
        activeSpwnCount--;
        if (activeSpwnCount == 0 && spawnListEmpty && GameStateManager.inAction)
        {
            GameStateManager.RequestSwitch(GameState.Success);
        }
    }

    public void SpawnImpactParticleAt(Vector3 pos, GameObject particle)
    {
        GameObject go= Pool.Instantiate(particle, pos, Quaternion.identity);
        Centralizer.Add_DelayedMonoAct(this, () =>
        {
            Pool.Destroy(go);
        }, 3);
    }
    public void SpawnImpactParticleAt(Transform tr, GameObject particle)
    {
        SpawnImpactParticleAt(tr.position,particle);
    }
}


[System.Serializable]
public class SpawnGroup
{
    public bool active = true;
    [Range(0,1.0f)]
    public float gapRatio = 0.5f;
    public float gapOffset = 1;
    //public float maxAdditionalRandomWait = 0;
    public List<EnemyMotion> enemies;

    public float LongestReachTime(float distance)
    {
        float longestTime = 0;
        foreach (EnemyMotion enmey in enemies)
        {
            float t = distance / enmey.moveSpeed;
            if (t > longestTime)
                longestTime = t;
        }
        return longestTime;
    }
}

//#if UNITY_EDITOR
//[CustomPropertyDrawer(typeof(SpawnGroup))]
//public class SpawnGroupDataDrawer : PropertyDrawer
//{
//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        label.text = "";
//        SpawnGroup spg =  GetParent(property);
//        bool active = property.FindPropertyRelative("active").boolValue;
//        float gapOffset = property.FindPropertyRelative("gapOffset").floatValue;
//        List<EnemyMotion> prefab = property.FindPropertyRelative("enemies") as List<GameObject>;
//        //int triggerSpawnAtCount = property.FindPropertyRelative("triggerSpawnAtCount").intValue;
//        //string id = property.FindPropertyRelative("id").stringValue;
//        //int analyticsID = property.FindPropertyRelative("analyticsID").intValue;
//        //int displayID = property.FindPropertyRelative("displayID").intValue;
//        //bool isBon = property.FindPropertyRelative("isBonus").boolValue;

//        string title;

//        float initialOffset = 0;
//        //if (isBon) initialOffset = 25;

//        //int serial = BuildSceneData.currentFocus.FindSerial(id);
//        //if (serial >= 0)
//        //{
//        //    if (!isBon)
//        //        title = string.Format("Level {0}", serial);
//        //    else
//        //        title = string.Format("Bonus {0}", serial);
//        //}
//        //else
//        //    title = string.Format("Serial NOT found");
//        if (prefab) title = prefab.name;
//        else title = "Empty";


//        EditorGUIUtility.labelWidth = initialOffset;

//        EditorGUI.BeginProperty(position, label, property);

//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

//        var indent = EditorGUI.indentLevel;
//        EditorGUI.indentLevel = 0;

//        float btnW = 20;
//        float spc = 2;
//        float elementHeight = position.height;

//        float offset = initialOffset;
//        var activeRect = new Rect(position.x + offset, position.y, 20, elementHeight);
//        offset += (20 + spc);
//        var titleRect = new Rect(position.x + offset, position.y, 120 - initialOffset, elementHeight);
//        offset += (120 - initialOffset + spc);
//        var counterRect = new Rect(position.x + offset, position.y, 50, elementHeight);
//        offset += (50 + spc);
//        var timerRect = new Rect(position.x + offset, position.y, 70, elementHeight);
//        offset += (70 + spc);

//        var refRect = new Rect(position.x + offset, position.y, position.width - offset - btnW * 2 - spc * 2, elementHeight);

//        //var removeRect = new Rect(position.x + position.width - btnW * 2 - spc, position.y, btnW, elementHeight);

//        //var addRect = new Rect(position.x + position.width - btnW, position.y, btnW, elementHeight);

//        EditorGUI.PropertyField(activeRect, property.FindPropertyRelative("active"), GUIContent.none);
//        EditorGUI.LabelField(titleRect, title);
//        EditorGUI.PropertyField(counterRect, property.FindPropertyRelative("triggerSpawnAtCount"), GUIContent.none);
//        EditorGUI.PropertyField(timerRect, property.FindPropertyRelative("gapOffset"), GUIContent.none);
//        EditorGUI.PropertyField(refRect, property.FindPropertyRelative("prefab"), GUIContent.none);

//        //if (BuildSceneData.currentFocus != null)
//        //{
//        //    if (GUI.Button(removeRect, "-"))
//        //    {
//        //        BuildSceneData.currentFocus.RemoveID(id);
//        //    }
//        //    if (GUI.Button(addRect, "+"))
//        //    {
//        //        BuildSceneData.currentFocus.AddElement(id);
//        //    }
//        //}
//        EditorGUI.indentLevel = indent;


//        EditorGUI.EndProperty();

//    }

//    //public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//    //{
//    //    return base.GetPropertyHeight(property, label) * 2 + 10;
//    //}

//    public object GetParent(SerializedProperty prop)
//    {
//        var path = prop.propertyPath.Replace(".Array.data[", "[");
//        object obj = prop.serializedObject.targetObject;
//        var elements = path.Split('.');
//        foreach (var element in elements.Take(elements.Length - 1))
//        {
//            if (element.Contains("["))
//            {
//                var elementName = element.Substring(0, element.IndexOf("["));
//                var index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
//                obj = GetValue(obj, elementName, index);
//            }
//            else
//            {
//                obj = GetValue(obj, element);
//            }
//        }
//        return obj;
//    }
//    public object GetValue(object source, string name)
//    {
//        if (source == null)
//            return null;
//        var type = source.GetType();
//        var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
//        if (f == null)
//        {
//            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
//            if (p == null)
//                return null;
//            return p.GetValue(source, null);
//        }
//        return f.GetValue(source);
//    }

//    public object GetValue(object source, string name, int index)
//    {
//        var enumerable = GetValue(source, name) as IEnumerable;
//        var enm = enumerable.GetEnumerator();
//        while (index-- >= 0)
//            enm.MoveNext();
//        return enm.Current;
//    }

//}
//#endif