﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class Enemy : MonoBehaviour
{
    public int score { get { return coins * 10; } }
    public int coins;
    public Agent selfAgent { get; private set; }
    Agent targetAgent;
    public Animator anim;
    public const float deathRemovalDelay = 0.5f;

    EnemyMotion motionScript;
    public void Init(System.Action onDeath)
    {
        selfAgent = GetComponent<Agent>();
        selfAgent.onDeath += ()=>
        {
            Player.Instance.OnKill(score,coins,this.transform);
            if (anim) anim.SetTrigger("death");

            onDeath?.Invoke();
            HandleDeath();
        };
        targetAgent = Player.Instance.GetComponent<Agent>();

        motionScript = GetComponent<EnemyMotion>();
        motionScript.Init(targetAgent.transform);
       
    }
    // Update is called once per frame
    void Update()
    {
        motionScript. UpdateMotion(selfAgent.alive);
    }

    void HandleDeath()
    {
        Centralizer.Add_DelayedMonoAct(SpawnGroupManager.Instance,
            () =>
            {
                Destroy(gameObject);
            }
            , deathRemovalDelay);
    }



}
