﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySprialMotion : EnemyMotion
{
    [Header("Spiral Motion Attributes")]
    public float radialSpeed = 50;
    
    //public float rotateSpeed_min = 5;
    //float rotateSpeed_max = 50;
    // Start is called before the first frame update
    //[RangeAttribute(0,2)]
    //public float A = 0.2f;
    //[RangeAttribute(0, 25)]
    //public float P = 10;
    //[RangeAttribute(0.5f, 10)]
    //public float standardRadius = 4;
    //[RangeAttribute(0, 100)]
    //public float angleBonusPercentage = 10;
    //[RangeAttribute(0, 100)]
    //public float angleBonusPercentage_outer = 50;

    //public float superMax = 360;


    //float refRotSpeed;
    public override void SetInitialTransform()
    {
        //this.transform.position = Player.Instance.GetSpawnPoint();

        SetFace(90);
    }
    public override void VoluntaryMotion(float slowMult, Vector3 toPlayerVec, float angle)
    {
        base.VoluntaryMotion(slowMult, toPlayerVec, angle);


        this.transform.position = this.transform.position + (FaceDir * radialSpeed * Time.deltaTime*slowMult);

        float radius = toPlayerVec.magnitude;
        float perfectAngularSpeed = (radialSpeed * 360 / (radius * 2 * Mathf.PI));


        //float bonusPercentage = (radius < standardRadius) ? angleBonusPercentage : angleBonusPercentage_outer;
        //float den = Mathf.Pow( (A*radius + 1),P);
        //float frac = 1 - (1/den);

        float rotateSpeed = perfectAngularSpeed*slowMult;// (Mathf.Abs(angle) > 90) ? rotateSpeed_max : Mathf.Lerp(superMax, rotateSpeed_min, frac);
        float rotateAngle;
        //Debug.LogFormat("{2}___{0} / {1}", radius, perfectAngularSpeed, angle);
        if (Mathf.Abs(angle - 180) < 1 || Mathf.Abs(angle + 180) < 1)
        {
            rotateAngle = rotateSpeed * Time.deltaTime;

            //Debug.Log("A___180");
        }
        else if (angle > 0)//do not merge with upper logic
        {
            //Debug.LogFormat("A___+");
            rotateAngle = rotateSpeed * Time.deltaTime;
            if (rotateAngle > angle) rotateAngle = angle;
        }
        else if (angle < 0)
        {
            //Debug.Log("A___-");
            rotateAngle = -rotateSpeed * Time.deltaTime;
            if (rotateAngle < angle) rotateAngle = angle;
        }
        else //rotat angle is 0
        {
            //Debug.Log("A___0");
            rotateAngle = 0;
        }

        this.transform.rotation = this.transform.rotation * Quaternion.Euler(0, 0, rotateAngle);


    }
}
