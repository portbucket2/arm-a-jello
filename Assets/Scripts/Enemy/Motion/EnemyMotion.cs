﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMotion : MonoBehaviour, IStatusAffectable
{
    public Transform alignable;
    public float moveSpeed = 2;

    public GameObject shockFX;
    protected Transform target;

    protected Vector3 FaceDir => transform.right;

    public void Init(Transform target)
    {
        this.target = target;
        SetInitialTransform();
    }
    public virtual void SetInitialTransform()
    {
        //this.transform.position = Player.Instance.GetSpawnPoint();
        SetFace(0);
        if (alignable)
        {
            float angle = Vector3.Angle(this.transform.up, Vector3.up);
            if(angle>90) alignable.localRotation = Quaternion.Euler(new Vector3(180,0,0));
            else alignable.localRotation = Quaternion.identity;
        }
    }
    protected void SetFace(float extraAngle = 0)
    {
        Vector3 playerDir = target.position - this.transform.position;
        playerDir.z = 0;
        float angle = Vector3.SignedAngle(FaceDir, playerDir, Vector3.forward);
        this.transform.rotation = this.transform.rotation * Quaternion.Euler(0, 0, angle + extraAngle);

    }

    void IStatusAffectable.StunFor(float duration)
    {
        stunnedTimeCharge = duration;
    }
    void IStatusAffectable.SlowFor(float duration)
    {
        slowedTimeCharge = duration;
    }
    void IStatusAffectable.AddKnockBack(float distance)
    {
        pushDistanceCharge = distance;
    }
    
    [Header("Effect Values")]
    [SerializeField] float stunnedTimeCharge = 0;
    [SerializeField] float slowedTimeCharge = 0;
    [SerializeField] float pushDistanceCharge = 0;
    bool isStunned => stunnedTimeCharge>0;
    bool isSlowed => slowedTimeCharge>0;
    bool isKnockBack => pushDistanceCharge > 0;
    public void UpdateMotion(bool voluntaryActive)
    {
        if (isStunned != shockFX.activeSelf) shockFX.SetActive(isStunned);
        float slowMult = 1;
        if (isSlowed)
        {
            slowMult = 0.35f;
            slowedTimeCharge -= Time.deltaTime;
        }
        else
        {
            slowMult = isKnockBack ? 0.5f : 1.0f;
        }

        Vector3 toPlayerVec = target.position - this.transform.position;
        if (isStunned)
        {
            stunnedTimeCharge -= Time.deltaTime;
        }
        else if (target && voluntaryActive)
        {
            toPlayerVec.z = 0;
            float angle = Vector3.SignedAngle(FaceDir, toPlayerVec, Vector3.forward);
            VoluntaryMotion(slowMult, toPlayerVec, angle);
        }


        if (pushDistanceCharge > 0)
        {
            float push = Mathf.Clamp( Mathf.Lerp(0, pushDistanceCharge, 5 * Time.deltaTime),0,pushDistanceCharge);
            if (push < 0.2f * Time.deltaTime)
            {
                pushDistanceCharge = 0;
            }
            else
            {
                pushDistanceCharge -= push;
                if (pushDistanceCharge < 0) pushDistanceCharge = 0;

                Vector3 movementVector = toPlayerVec.normalized *push;
                this.transform.position = this.transform.position - movementVector;
            }
        }
    }

    public virtual void VoluntaryMotion(float slowMult, Vector3 toPlayerVec, float angle)
    {
        Vector3 movementVector = toPlayerVec.normalized * moveSpeed * Time.deltaTime * slowMult;
        this.transform.position = this.transform.position + movementVector;
    }
}
interface IStatusAffectable
{
    void StunFor(float duration);
    void SlowFor(float duration);
    void AddKnockBack(float distance);
}