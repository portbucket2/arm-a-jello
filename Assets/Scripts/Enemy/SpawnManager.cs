﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class SpawnManager : MonoBehaviour
{
    public static System.Action<float> levelProgressUpdated;

    public float progress { get { return deathCount / spawns.Count; } }
    public List<Spawn> spawns;
    [Header("Observe Only")]
    [SerializeField] int activeSpwnCount = 0;

    [SerializeField] bool spawnListEmpty;
    IEnumerator Start()
    {
        deathCount = 0;
        spawnListEmpty = false;
        for (int i = 0; i < spawns.Count; i++)
        {
            Spawn spawn = spawns[i];
            if (!spawn.active) continue ;
            while (activeSpwnCount > spawn.triggerSpawnAtCount || !GameStateManager.inAction)
            {
                yield return null;
            }
            yield return new WaitForSeconds(spawn.minTime);
            SpawnAgent(spawn);
        }
        spawnListEmpty = true;
    }

    void SpawnAgent(Spawn spawn)
    {
        GameObject go =  Instantiate(spawn.prefab,Vector3.zero,spawn.prefab.transform.rotation);
        Enemy enemy = go.GetComponent<Enemy>();
        enemy.Init(OnEnemyDeath);
        activeSpwnCount++;
    }
    float deathCount = 0;

    void OnEnemyDeath()
    {
        deathCount++;
        levelProgressUpdated?.Invoke(progress);
        activeSpwnCount--;

        if (activeSpwnCount == 0 && spawnListEmpty && GameStateManager.inAction) 
        {
            GameStateManager.RequestSwitch(GameState.Success);
        }
    }

}

[System.Serializable]
public class Spawn
{
    public bool active = true;
    public GameObject prefab;
    public int triggerSpawnAtCount=100;
    public float minTime= 0.5f;
}

