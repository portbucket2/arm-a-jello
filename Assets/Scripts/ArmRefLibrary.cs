﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRefLibrary : MonoBehaviour
{
    public List<AttackArm> armPrefabRefs;
    public static Dictionary<ArmType,AttackArm> armDictionary;
    private void Awake()
    {
        if (armDictionary==null && armPrefabRefs.Count>0)
        {
            armDictionary = new Dictionary<ArmType, AttackArm>();
            foreach (AttackArm aa in armPrefabRefs)
            {
                armDictionary.Add(aa.profile.armType,aa);
            }
        }
    }
    public static GameObject GetPrefab(ArmType type)
    {
        return armDictionary[type].gameObject;
    }
    public static Sprite GetProPic(ArmType type)
    {
        return armDictionary[type].profileImage;
    }

}
