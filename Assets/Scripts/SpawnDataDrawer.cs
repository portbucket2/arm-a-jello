﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomPropertyDrawer(typeof(Spawn))]
public class SpawnDataDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        label.text = "";
        bool active = property.FindPropertyRelative("active").boolValue;
        GameObject prefab = property.FindPropertyRelative("prefab").objectReferenceValue as GameObject;
        int triggerSpawnAtCount=property.FindPropertyRelative("triggerSpawnAtCount").intValue;
        float minTime = property.FindPropertyRelative("minTime").floatValue;
        //string id = property.FindPropertyRelative("id").stringValue;
        //int analyticsID = property.FindPropertyRelative("analyticsID").intValue;
        //int displayID = property.FindPropertyRelative("displayID").intValue;
        //bool isBon = property.FindPropertyRelative("isBonus").boolValue;
        
        string title;

        float initialOffset = 0;
        //if (isBon) initialOffset = 25;

        //int serial = BuildSceneData.currentFocus.FindSerial(id);
        //if (serial >= 0)
        //{
        //    if (!isBon)
        //        title = string.Format("Level {0}", serial);
        //    else
        //        title = string.Format("Bonus {0}", serial);
        //}
        //else
        //    title = string.Format("Serial NOT found");
        if (prefab) title = prefab.name;
        else title = "Empty";


        EditorGUIUtility.labelWidth = initialOffset;

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        float btnW = 20;
        float spc = 2;
        float elementHeight = position.height;

        float offset = initialOffset;
        var activeRect = new Rect(position.x + offset, position.y, 20, elementHeight);
        offset += (20 + spc);
        var titleRect = new Rect(position.x + offset, position.y, 120 - initialOffset, elementHeight);
        offset += (120 - initialOffset + spc);
        var counterRect = new Rect(position.x + offset, position.y, 50, elementHeight);
        offset += (50 + spc);
        var timerRect = new Rect(position.x + offset, position.y, 70, elementHeight);
        offset += (70 + spc);

        var refRect = new Rect(position.x + offset, position.y, position.width - offset - btnW * 2 - spc * 2, elementHeight);

        //var removeRect = new Rect(position.x + position.width - btnW * 2 - spc, position.y, btnW, elementHeight);

        //var addRect = new Rect(position.x + position.width - btnW, position.y, btnW, elementHeight);

        EditorGUI.PropertyField(activeRect, property.FindPropertyRelative("active"), GUIContent.none);
        EditorGUI.LabelField(titleRect, title);
        EditorGUI.PropertyField(counterRect, property.FindPropertyRelative("triggerSpawnAtCount"), GUIContent.none);
        EditorGUI.PropertyField(timerRect, property.FindPropertyRelative("minTime"), GUIContent.none);
        EditorGUI.PropertyField(refRect, property.FindPropertyRelative("prefab"), GUIContent.none);

        //if (BuildSceneData.currentFocus != null)
        //{
        //    if (GUI.Button(removeRect, "-"))
        //    {
        //        BuildSceneData.currentFocus.RemoveID(id);
        //    }
        //    if (GUI.Button(addRect, "+"))
        //    {
        //        BuildSceneData.currentFocus.AddElement(id);
        //    }
        //}
        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    //public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    //{
    //    return base.GetPropertyHeight(property, label) * 2 + 10;
    //}

}
#endif