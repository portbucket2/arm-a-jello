﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class ShopManager : MonoBehaviour
{
    public static ShopManager Instance { get; private set; }
    public List<ShopItemLoader> shopLoader;
    // Start is called before the first frame update
    public TextAsset upgradeText;
    public TextAsset armText;

    List<ArmPurchaseData> armDataList = new List<ArmPurchaseData>();
    List<UpgradePurchaseData> upgradeDataList = new List<UpgradePurchaseData>();
    public GameObject shopRoot;
    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        CSVReader.Parse_toClassWithBasicConstructor(armDataList, armText);
        CSVReader.Parse_toClassWithBasicConstructor(upgradeDataList, upgradeText);
        RefreshRosterSlots(EmptySlotsOnly: true);

        if (GameLevelManager.Instance.LevelNumberLatest == 1)
        {
            shopRoot.SetActive(false);
        }
        else
        {
            LoadShopForLevel();

            GameStateManager.stateAccess.AddStateExitCallback(GameState.Purchase, LoadShopForLevel);
        }
    }

    // Update is called once per frame
    public static void RequestRefreshOnFail()
    {
        LastLoadedShopLevel.value--;
    }
    void LoadShopForLevel()
    {
        if (GameLevelManager.Instance.LevelNumberLatest > LastLoadedShopLevel.value)
        {
            RefreshRosterSlots(EmptySlotsOnly: false);//refresh all slots
        }

        bool newArmOnSale = PlayerDataController.ArmCount < armDataList.Count;

        for (int i = 0; i < 3; i++)
        {
            if (newArmOnSale && i == 2)
            {
                ArmPurchaseData apd = armDataList[PlayerDataController.ArmCount];
                shopLoader[i].Load(ArmRefLibrary.GetProPic(ArmType.Basic), "Extra Arm", "More Arm More Mayhem", apd.cost,()=> 
                {
                    OnItemClick(ArmType.Basic,apd.cost);
                });
            }
            else
            {
                UpgradePurchaseData upd = GetDataForSlot(i);
                if (upd != null)
                {
                    shopLoader[i].Load(ArmRefLibrary.GetProPic(upd.type), upd.title, upd.description, upd.cost, () =>
                    {
                        OnItemClick(upd.type,upd.cost);
                    });
                }
                else
                {
                    shopLoader[i].LoadUnavailable();
                }

            }
        }
    }
    void OnItemClick(ArmType type, int cost)
    {
        PlayerDataController.Instance.UpgradeRequest(type, () =>
        {
            Currency.Transaction(CurrencyType.CASH, -cost);
            LoadShopForLevel();//sequence sensitive
            GameStateManager.RequestSwitch(GameState.Init);
        });
    }
    static UpgradePurchaseData GetDataForSlot(int index)
    {
        int typeID = UpgradeRoster[index].value;
        if (typeID < 0)
        {
            //Debug.Log("A");
            return null;
        }
        else
        {
            foreach (UpgradePurchaseData upd in Instance.upgradeDataList)
            {
                if (upd.typeIndex == typeID)
                {
                    //Debug.Log("B");
                    return upd;
                }
            }
        }
        //Debug.Log("C");
        return null;
    }
    static void RefreshRosterSlots(bool EmptySlotsOnly)
    {
        if(!EmptySlotsOnly) LastLoadedShopLevel.value = GameLevelManager.Instance.LevelNumberLatest;
        LoadChances(GameLevelManager.Instance.LevelNumberLatest);
        for (int i = 0; i < 3; i++)
        {
            if (UpgradeRoster[i].value < 0 || !EmptySlotsOnly)
            {
                RefreshRosterSlot(i);
            }
        } 
    }

    static void RefreshRosterSlot(int index)
    {
        if (chances.items.Count > 0)
        {
            UpgradePurchaseData upd = chances.Roll(chanceReformMultiplier: 0.1f);
            //Debug.Log(upd.title);
            UpgradeRoster[index].value = upd.typeIndex;
        }
        else
        {
            //Debug.Log("0 chances");
            UpgradeRoster[index].value = -1;
        }
    }
    private static ChancedList<UpgradePurchaseData> chances;
    static void LoadChances(int forLevel)
    {
        if (chances == null) chances = new ChancedList<UpgradePurchaseData>();
        chances.Clear();
        //Debug.Log(Instance.upgradeDataList.Count);
        foreach (UpgradePurchaseData upd in Instance.upgradeDataList)
        {
            //Debug.LogFormat("{0}<={1}", upd.minLevel,forLevel);
            if (upd.minLevel <= forLevel) chances.Add(upd, upd.minLevel);
        }
    }
    private static List<HardData<int>> _upgradeRoster;
    private static List<HardData<int>> UpgradeRoster
    {
        get
        {
            if (_upgradeRoster == null)
            {
                _upgradeRoster = new List<HardData<int>>();
                for (int i = 0; i < 3; i++)
                {
                    HardData<int> rosterSlot = new HardData<int>(string.Format("SHOP_ROST_{0}", i), -1);
                    _upgradeRoster.Add(rosterSlot);
                }
            }
            return _upgradeRoster;
        }
    }
    private static HardData<int> _lastLoadedShopLevel;
    private static HardData<int> LastLoadedShopLevel
    {
        get
        {
            if (_lastLoadedShopLevel == null) _lastLoadedShopLevel = new HardData<int>("LAST_SHOP_LEVEL", -1);
            return _lastLoadedShopLevel;
        }
    }
}

[System.Serializable]
public class UpgradePurchaseData 
{
    public int typeIndex;
    public string title;
    public string description;
    public int cost;
    public int minLevel;
    public ArmType type { get { return (ArmType)typeIndex; } }
}
[System.Serializable]
public class ArmPurchaseData
{
    public int armNumber;
    public int cost;
}