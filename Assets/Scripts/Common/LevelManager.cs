﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.faithstudio.SDK;

public class LevelManager : GameLevelManager
{
    public int LevelCount;
    public override int GetBuildIndexForLevelIndex(int levelIndex)
    {
        return (levelIndex%LevelCount) + 1;
    }


    IEnumerator Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            onLevelLoaded += (int i) =>
            {
                Debug.LogFormat("Test Event Sent for level {0}",i);
            };
            LoadLatest();
            yield return null;
            ReportLevelStart();
        }
    }
    public static void ReportLevelStart()
    {
        FacebookAnalyticsManager.Instance.FBALevelStart(Instance.LevelNumberCurrent);
    }
    public static void ReportLevelRestart()
    {
        FacebookAnalyticsManager.Instance.FBALevelRestart(Instance.LevelNumberCurrent);
    }
    public static void ReportLevelSuccess()
    {
        FacebookAnalyticsManager.Instance.FBALevelComplete(Instance.LevelNumberCurrent);
    }
    public static void ReportLevelFail()

    {
        FacebookAnalyticsManager.Instance.FBALevelFailed(Instance.LevelNumberCurrent);
    }
}
