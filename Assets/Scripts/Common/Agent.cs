﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Agent : MonoBehaviour
{
    public float HP = 3;
    public bool alive = true;
    public float estimatedRadius= 0.5f;
    float maxHP;


    public GameObject weakDamageParticle;
    public GameObject heavyDamageParticle;

    private void Awake()
    {
        maxHP = HP;
    }

    public event Action onDeath;
    public event Action onDamage;
    public void ChangeHP(float change, DamageFXType dmgFX)
    {
        if (!alive) return;
        //Debug.LogFormat("{0} - {1}", HP, change);
        HP += change;
        if (change < 0)
        {
            switch (dmgFX)
            {
                case DamageFXType.WEAK:
                    SpawnGroupManager.Instance.SpawnImpactParticleAt(transform.position + new Vector3(0, 0, -1), weakDamageParticle);
                    break;
                case DamageFXType.HEAVY:
                    SpawnGroupManager.Instance.SpawnImpactParticleAt(transform.position + new Vector3(0, 0, -1), heavyDamageParticle);
                    break;
            }
            onDamage?.Invoke();
        }
        HP = Mathf.Clamp(HP,0,maxHP);
        if (HP <= 0)
        {
            alive = false;
            //Debug.Log("Death");
            onDeath?.Invoke();
        }
    }


}

public enum DamageFXType
{
    NONE =0,
    WEAK =1,
    HEAVY = 2,
}