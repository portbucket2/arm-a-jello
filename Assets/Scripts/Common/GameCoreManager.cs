﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System.Reflection;

public class GameCoreManager : MonoBehaviour
{
    public static GameCoreManager Instance { get; private set; }
    public GameObject cashUpPrefab;
    public int scoreEarnedInThisRun;
    public int coinsEarnedInThisRun;
    public int TotalCoinForWin { get { return coinsEarnedInThisRun * 2; } }
    public void ClaimBonusCoins()
    {
        AddCoin(TotalCoinForWin- coinsEarnedInThisRun);
    }
    public static event System.Action<int> onScoreUpdated;


    void Start()
    {

        Instance = this;
        scoreEarnedInThisRun = 0;
        coinsEarnedInThisRun = 0;
        AddScoreEarned(0);
    }
    void AddScoreEarned(int earned)
    {
        scoreEarnedInThisRun += earned;
        onScoreUpdated?.Invoke(scoreEarnedInThisRun);
    }
    public static void AddScore(int earned)
    {
        Instance.AddScoreEarned(earned);
    }
    public static void AddCoin(int earned, Transform animateTr=null)
    {
        //Debug.Log(earned);
        Instance.coinsEarnedInThisRun += earned;
        Currency.Transaction(CurrencyType.CASH, earned);
        if (animateTr)
        {
            GameObject go = Pool.Instantiate(Instance.cashUpPrefab, animateTr.position, Instance.cashUpPrefab.transform.rotation);
            go.GetComponent<UI_CashUp>().cashText.text = string.Format("+{0}",earned);
            Centralizer.Add_DelayedMonoAct(Instance, () =>
             {
                 Pool.Destroy(go);
             }, 2);
        }
    }
}
