﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideTapDetector : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        abc = GetComponent<ArmBase>();
    }

     ArmBase abc;
    // Update is called once per frame
    void Update()
    {
        float val = 0;
        if (Application.isEditor)
        {
            if (Input.GetMouseButton(0))
            {

                val = -1;
            }
            else if (Input.GetMouseButton(1))
            {
                val = 1;
            }
        }
        else 
        {
            if (Input.GetMouseButton(0))
            {
                if (Input.mousePosition.x > Screen.width / 2)
                {
                    val = 1;

                }
                else
                {
                    val = -1;
                }
            }
        }

        abc.SetRotMult(val);
    }
}
