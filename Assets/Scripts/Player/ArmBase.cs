﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmBase : MonoBehaviour
{
    public static int ArmGap { get { return 360 / PlayerDataController.ArmCount; } }
    public List<AttackArm> arms;
    public void Init(List<ArmDataKeep> armDatas)
    {
        foreach (Transform tr in this.transform)
        {
            Destroy(tr.gameObject);
        }
        arms.Clear();

        for (int i = 0; i < armDatas.Count; i++)
        {
            AddFreshArm(armDatas[i]);
        }
    }
    public void TurnOnNewArmMode(System.Action<int> onButton)
    {
        for (int i = 0; i < arms.Count; i++)
        {
            int index = arms[i].dataRef.posIndex;
            arms[i].pointer.Load(false, -ArmGap / 2, () =>
                {
                    onButton?.Invoke(index);
                });
        }
    }
    public void TurnOnUpgradeMode(System.Action<int> onButton)
    {
        for (int i = 0; i < arms.Count; i++)
        {
            int index = arms[i].dataRef.posIndex;
            arms[i].pointer.Load(true, 0, () =>
            {
                onButton?.Invoke(index);
            });
        }
    }
    public void TurnOffPointers()
    {
        for (int i = 0; i < arms.Count; i++)
        {
            arms[i].pointer.TurnOff();
        }
    }

    public AttackArm AddFreshArm(ArmDataKeep adk)
    {
        GameObject prefab = adk.GetArmPrefab();
        if (prefab)
        {
            GameObject freshArm = Instantiate(prefab, this.transform);
            freshArm.name = adk.installationSerial.ToString();
            AttackArm aa = freshArm.GetComponent<AttackArm>();
            aa.Init(adk);
            arms.Add(aa);
            Transform armiT = freshArm.transform;
            armiT.localPosition = Vector3.zero;
            armiT.localScale = Vector3.one;
            armiT.localRotation = Quaternion.identity * Quaternion.Euler(armiT.forward * ArmGap * adk.posIndex);
            return aa;
        }
        else
        {
            Debug.LogError("Arm prefab not found!");
            return null;
        }
    }
    public void ReplaceArm(ArmDataKeep adk)
    {
        AttackArm prevArm = FindArmInPosition(adk.posIndex);//SequenceSensiteve
        arms.Remove(prevArm);
        Destroy(prevArm.gameObject);
        AddFreshArm(adk);
    }

    public void AddNewArmMidGame(ArmDataKeep adk)
    {
        AttackArm currentArm = AddFreshArm(adk);//SequenceSensiteve
        AttackArm prevArm = FindArmInPosition(adk.posIndex - 1);//SequenceSensiteve
        AttackArm nextArm = FindArmInPosition(adk.posIndex + 1);//SequenceSensiteve
        currentArm.transform.localRotation = Quaternion.Slerp(prevArm.transform.localRotation,nextArm.transform.localRotation,0.5f);
        StartCoroutine(SlerpToNewRots());
    }
    AttackArm FindArmInPosition(int posIndex)
    {
        if (posIndex == -1) posIndex = PlayerDataController.ArmCount -1;
        if (posIndex == PlayerDataController.ArmCount) posIndex = 0;

        foreach (AttackArm aa in arms)
        {
            if (aa.dataRef.posIndex == posIndex) return aa;
        }
        return null;
    }
    IEnumerator SlerpToNewRots()
    {
        float timeToAdjust=2f;
        float timePast = 0;
        float gap = 360 / arms.Count;
        do
        {
            timePast += Time.deltaTime;
            for (int i = 0; i < arms.Count; i++)
            {
                Transform armiT = arms[i].transform;
                Quaternion targetRot = Quaternion.identity * Quaternion.Euler(armiT.forward * gap * arms[i].dataRef.posIndex);
                armiT.localRotation = Quaternion.Slerp(armiT.localRotation, targetRot, Mathf.Clamp01(timePast / timeToAdjust));
            }
            yield return null;
        } while (timePast <= timeToAdjust);
    }

    //void Start()
    //{
    //    SetArmPositions();
    //}

    #region rotation
    [Header("Rotation Settings")]
    public float rotationSpeed = 120;
    [Header("SPEC only")]
    [SerializeField] float targetRotMult;
    [SerializeField] float rotMult;
    [SerializeField] float accumulatedAngle;
    public void SetRotMult(float mult)
    {
        //Debug.LogError("this function doesnt do anything right now!");
        if (GameStateManager.inAction)
        {

            targetRotMult = mult;
        }
    }
    void OnUpdateOld()
    {

        rotMult = Mathf.Lerp(rotMult, targetRotMult, 10 * Time.deltaTime);
        transform.Rotate(Vector3.forward, rotationSpeed * rotMult * Time.deltaTime);
    }


    public void AddRot(float angle)
    {
        accumulatedAngle += angle;
    }
    //void OnUpdateNew()
    //{
    //    float maxRot = rotationSpeed * Time.deltaTime;
    //    float rotAngle = Mathf.Clamp(accumulatedAngle, -maxRot, maxRot);
    //    accumulatedAngle -= rotAngle;
    //    transform.Rotate(Vector3.forward, rotAngle );
    //}
    private void Update()
    {
        OnUpdateOld();
        //OnUpdateNew();
    }
    #endregion
}
