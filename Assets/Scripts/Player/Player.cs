﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }
    public Transform spawnPointsRoot;
    public List<Transform> spawnPoints;
    public Animator anim;
    private Agent selfAgent;

    //public List<GameObject> moods;
    void Awake()
    {
        Instance = this;
        selfAgent = GetComponent<Agent>();
        selfAgent.onDamage += () =>
        {
            GameStateManager.RequestSwitch(GameState.Fail);
            anim.SetTrigger("death");
        };
        foreach (Transform child in spawnPointsRoot)
        {
            spawnPoints.Add(child);
        }
    }
    public void OnKill(int score, int coins, Transform coinPoint)
    {
        if (!selfAgent.alive) return;
        GameCoreManager.AddScore(score);
        GameCoreManager.AddCoin(coins, coinPoint);
        anim.SetTrigger("kill");
    }
    public Vector3 GetSpawnPoint()
    {
        return  spawnPoints[Random.Range(0, spawnPoints.Count)].position;
    }
}
