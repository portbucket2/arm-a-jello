﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class PlayerDataController : MonoBehaviour
{
    public const int MIN_ARM_COUNT = 2;
    public const int MAX_ARM_COUNT = 6;
    public static int ArmCount { get { return currentArmCount.value; } }
    public static HardData<int> currentArmCount;
    private static List<ArmDataKeep> armDKlist;

    public static PlayerDataController Instance { get; private set; }

    public ArmBase armBase;

    private void Awake()
    {
        Instance = this;
        Init();
    }
    private void Start()
    {
        GameStateManager.stateAccess.AddStateEntryCallback(GameState.Purchase, () =>
        {

            if (installationArmType == ArmType.Basic) 
                armBase.TurnOnNewArmMode(ConfirmNewArm);
            else
                armBase.TurnOnUpgradeMode(ConfirmUpgrade);
        }); 
        GameStateManager.stateAccess.AddStateExitCallback(GameState.Purchase, () =>
        {
            armBase.TurnOffPointers();
        });
    }
    ArmType installationArmType;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            installationArmType = ArmType.Basic;
            GameStateManager.RequestSwitch(GameState.Purchase);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            installationArmType = ArmType.Star;
            GameStateManager.RequestSwitch(GameState.Purchase);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            installationArmType = ArmType.Eel;
            GameStateManager.RequestSwitch(GameState.Purchase);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            installationArmType = ArmType.Sonar;
            GameStateManager.RequestSwitch(GameState.Purchase);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            installationArmType = ArmType.Crab;
            GameStateManager.RequestSwitch(GameState.Purchase);
        }
        for (int i = 0; i < 6; i++)
        {
            if (Input.GetKeyDown((KeyCode)(((int)KeyCode.Keypad0) + i)))
            {
                installationArmType = (ArmType) i ;
                GameStateManager.RequestSwitch(GameState.Purchase);
                onPurchaseConfirmed = null;
            }
        }
    }

    System.Action onPurchaseConfirmed;
    public void UpgradeRequest(ArmType type, System.Action onPurchaseConfirmed)
    {
        this.onPurchaseConfirmed = onPurchaseConfirmed;//overwrite
        installationArmType = type;
        GameStateManager.RequestSwitch(GameState.Purchase);
    }

    static void Init()
    {
        if (armDKlist == null)
        {
            currentArmCount = new HardData<int>("ARM_COUNT", MIN_ARM_COUNT);
            armDKlist = new List<ArmDataKeep>(); 
            for (int i = 0; i < currentArmCount; i++)
            {
                ArmDataKeep adk = new ArmDataKeep(installationSerial: i);
                armDKlist.Add(adk);
            }
        }

        Instance.armBase.Init(armDKlist);
    }
   
    public void ConfirmNewArm(int preferredArmPosIndex)
    {
        Debug.LogFormat("New Arm At index {0}",preferredArmPosIndex);
        foreach (ArmDataKeep adk in armDKlist)
        {
            if (adk.posIndex >= preferredArmPosIndex) adk.ForceSetNewPos(adk.posIndex + 1);
        }

        ArmDataKeep freshArmForPosition = new ArmDataKeep(installationSerial: currentArmCount.value);
        freshArmForPosition.ForceSetNewType(installationArmType);
        freshArmForPosition.ForceSetNewPos(preferredArmPosIndex);
        currentArmCount.value++;
        armDKlist.Add(freshArmForPosition);
        Instance.armBase.AddNewArmMidGame(freshArmForPosition);
        GameStateManager.RequestSwitch(GameState.Init);
        onPurchaseConfirmed?.Invoke();
    }
    public void ConfirmUpgrade(int preferredArmPosIndex)
    {
        //Debug.LogFormat("Upgrade At index {0}", preferredArmPosIndex);
        foreach (ArmDataKeep adk in armDKlist)
        {
            if (adk.posIndex == preferredArmPosIndex)
            {
                adk.ForceSetNewType(installationArmType);
                Instance.armBase.ReplaceArm(adk);
            }
        }
        onPurchaseConfirmed?.Invoke();
    }

}
public class ArmDataKeep
{
    HardData<int> armPositionIndex;
    HardData<int> armTypeID;
    public int installationSerial { get; private set; }
    public ArmType Type { get { return (ArmType) armTypeID.value; } }
    public int posIndex { get { return armPositionIndex.value; } }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="installationSerial">installation serial number</param>
    public ArmDataKeep(int installationSerial)
    {
        this.installationSerial = installationSerial;
        armPositionIndex = new HardData<int>(string.Format("ARM_POS_{0}",installationSerial), installationSerial);//serial index is default position until swapped
        armTypeID = new HardData<int>(string.Format("ARM_TYP_{0}", installationSerial), (int) ArmType.Basic);
    }

    public GameObject GetArmPrefab() 
    {
        return ArmRefLibrary.GetPrefab(Type);
        //string path = string.Format("Arms/{0}", Type);
        //return Resources.Load<GameObject>(path);
    }

    public void ForceSetNewPos(int posIndex)
    {
        armPositionIndex.value = posIndex;
    }
    public void ForceSetNewType(ArmType type)
    {
        armTypeID.value = (int) type;
    }
}
