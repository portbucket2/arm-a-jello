﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlReposition : MonoBehaviour
{
    public ParticleSystem ps;
    public Vector3 variance=new Vector3(1,0,0);
    public float durationOffset=-0.1f;
    IEnumerator Start()
    {
        ParticleSystem.EmissionModule em = ps.emission;
        while (true)
        {
            ps.transform.localPosition = new Vector3(Random.Range(-variance.x, variance.x), Random.Range(-variance.y, variance.y), Random.Range(-variance.z, variance.z));
            em.enabled = true;
            yield return new WaitForSeconds(ps.main.duration+durationOffset);
            em.enabled = false;
            yield return null;
        }
    }

}
