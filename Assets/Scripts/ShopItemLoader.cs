﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemLoader : MonoBehaviour
{
    //public Color bgEnabledColor;
    //public Color bgDisabledColr;

    public Image icon;
    public Text titleText;
    public Text descriptionText;
    public Text costText;
    public Button buyButton;

    public Color enoughColor = Color.white;
    public Color notEnoughColor = Color.red/2;
    public void Load(Sprite sp, string title, string description, int cost, System.Action onClick)
    {
        bool hasEnough = cost <= Currency.Balance(CurrencyType.CASH);
        buyButton.interactable = hasEnough;
        buyButton.onClick.RemoveAllListeners();
        buyButton.onClick.AddListener(()=> { onClick?.Invoke(); });
        titleText.text = title;
        descriptionText.text = description;
        costText.text = cost.ToString();
        costText.color = hasEnough ? enoughColor : notEnoughColor;
        titleText.color = hasEnough ? enoughColor : notEnoughColor;

        icon.sprite = sp;
    }

    public void LoadUnavailable()
    {
        
    }
}
