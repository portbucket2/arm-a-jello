﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_PurchaseState : MonoBehaviour
{
    public Button cancelPurchase;
    // Start is called before the first frame update
    void Start()
    {
        cancelPurchase.onClick.AddListener(() =>
        {
            GameStateManager.RequestSwitch(GameState.Init);
        });   
    }

}
