﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Start : MonoBehaviour
{
    public Button startButton;
    public GameObject startUIObject;
    // Start is called before the first frame update
    void Start()
    {
        startUIObject.SetActive(true);
        startButton.onClick.AddListener(() =>
        {
            if (GameStateManager.State == GameState.Init)
            {
                GameStateManager.RequestSwitch(GameState.Action);
                //startUIObject.SetActive(false);
            }
        });
    }

}
