﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UI_InGame : MonoBehaviour
{
    public Image progressBar;
    public Text scoreText;
    public Text levelNumberText;
    public GameObject idleTutorial;
    // Start is called before the first frame update
    void Start()
    {
        levelNumberText.text = string.Format("Level {0}",LevelManager.Instance.LevelNumberCurrent);
        progressBar.fillAmount = 0;
        scoreText.text = string.Format("{0}",0) ;
        SpawnGroupManager.levelProgressUpdated += OnProg;
        GameCoreManager.onScoreUpdated += OnScore;
        DragReporter.onIdleFlagChanged += OnIdleFlag;
        GameStateManager.stateAccess.AddStateExitCallback(GameState.Action, () =>OnIdleFlag(false));
    }
    private void OnDestroy()
    {
        SpawnGroupManager.levelProgressUpdated -= OnProg;
        GameCoreManager.onScoreUpdated -= OnScore;
        DragReporter.onIdleFlagChanged -= OnIdleFlag;
    }

    // Update is called once per frame
    void OnProg(float p)
    {
        progressBar.fillAmount = p;
    }
    void OnScore(int s)
    {
        scoreText.text = string.Format("{0}", s);
    }
    void OnIdleFlag(bool b)
    {
        idleTutorial.SetActive(b && GameStateManager.State == GameState.Action);
    }
}
