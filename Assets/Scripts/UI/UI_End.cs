﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_End : MonoBehaviour
{
    public GameState finalState;
    public GameObject panelRoot;
    public Text scoreText;
    public Text coinText;
    public Text bonusCoinText;
    public Button defaultButton;
    public Button videoButton;
    // Start is called before the first frame update
    void Start()
    {
        panelRoot.SetActive(false);
        defaultButton.onClick.AddListener(OnDefaultButton);
        videoButton.onClick.AddListener(OnVideoCallBack); //video callback needs
        // load default button callback
        // load video button callback

        GameStateManager.stateAccess.AddTranstionCallback(GameState.Action,finalState, () =>
        {
            FRIA.Centralizer.Add_DelayedMonoAct(this,()=>
            {

                panelRoot.SetActive(true);
                Debug.Log(GameCoreManager.Instance.coinsEarnedInThisRun);
                scoreText.text = GameCoreManager.Instance.scoreEarnedInThisRun.ToString();
                coinText.text = GameCoreManager.Instance.coinsEarnedInThisRun.ToString();
                if (bonusCoinText) bonusCoinText.text = GameCoreManager.Instance.TotalCoinForWin.ToString();

                if (finalState == GameState.Success)
                {
                    LevelManager.ReportLevelSuccess();
                    GameLevelManager.Instance.UnlockNextLevel();
                    GameCoreManager.Instance.ClaimBonusCoins();//sequence sensitive
                }
                else if (finalState == GameState.Fail)
                {
                    LevelManager.ReportLevelFail();
                    ShopManager.RequestRefreshOnFail();
                }
            },1.5f);

        });
    }

    void OnVideoCallBack()
    {
        if (finalState == GameState.Success)
        {
            DoubleCoin();
        }
        else if (finalState == GameState.Fail)
        {
            ExtraLife();
        }
    }
    void OnDefaultButton()
    {
        if (finalState == GameState.Success)
        {
            LevelManager.ReportLevelStart();
            Continue();
        }
        else if (finalState == GameState.Fail)
        {
            LevelManager.ReportLevelRestart();
            Retry();
        }
    }
    void Retry()
    {
        GameLevelManager.Instance.LoadCurrent();
    }
    void Continue()
    {
        GameLevelManager.Instance.LoadNext(forceUnlock: false);
    }

    void DoubleCoin()
    {
        Debug.Log("2x");
    }
    void ExtraLife()
    {
        Debug.Log("+1");
    }
}
