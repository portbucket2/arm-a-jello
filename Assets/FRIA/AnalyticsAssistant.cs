﻿using System.Collections.Generic;
using UnityEngine;
//using LionStudios;
 /*public static class AnalyticsAssistant 
{
   
    #if UNITY_EDITOR
        static bool logToConsole = true;
    #else
        static bool logToConsole = false;
    #endif
        static void Console(string format, params object[] args)
        {
            if (logToConsole) Debug.LogFormat(format, args);
        }

        public static void LevelStarted(int levelNo)
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("level", levelNo);
                Analytics.Events.LevelStarted(dic);
                Console("Started Level {0}", levelNo);
            }
            catch(System.Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
        public static void LevelCompleted(int levelNo,TaskProfile profile)
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("level", levelNo);
                string gameTitle = profile != null ? profile.taskID : "";
                dic.Add("minigame", gameTitle);
                Analytics.Events.LevelComplete(dic);
                Console("Completed Level {0}, gameplay: {1}", levelNo, gameTitle);
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
        }


        public static void MinigameStarted(TaskProfile profile)
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("minigame", profile.taskID);
                dic.Add("playcount", profile.playcount);
                Analytics.LogEvent("minigame_started", dic);
                Console("Started minigame {0}, playcount: {1}", profile.taskID, profile.playcount);
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
        public static void MinigameCompleted(TaskProfile profile)
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("minigame", profile.taskID);
                dic.Add("playcount", profile.playcount);
                Analytics.LogEvent("minigame_completed", dic);
                Console("Completed minigame {0}, playcount: {1}", profile.taskID, profile.playcount);
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
        }


        //    public static void LogRv(RVState rvState, RVPlacement placement, int level = -1)
        //    {
        //        switch (rvState)
        //        {
        //            case RVState.rv_click:
        //                LifetimeEvents.Report_RVClicked();
        //                break;
        //            case RVState.rv_start:
        //                break;
        //            case RVState.rv_complete:
        //                LifetimeEvents.Report_RVWatched();
        //                break;
        //            default:
        //                break;
        //        }
        //        if(level <0) level = LevelManager.SelectedLevelNumber;
        //        Log(string.Format("RVLog {0} lvl:{1}, placement: {2}", rvState.ToString(),level, placement.ToString()));
        //        AnalyticsController.LogRewardedVideos(rvState.ToString(), level, placement.ToString());
        //    }
        //    static Dictionary<string, string> adjust_EventToken_Dictionary;
        //    public static void LogLifeTimeEvent(string lifeTimeEventName)
        //    {
        //        if (adjust_EventToken_Dictionary == null)//initialize token dictionary
        //        {
        //            adjust_EventToken_Dictionary = new Dictionary<string, string>();
        //#if UNITY_ANDROID
        //            adjust_EventToken_Dictionary.Add("d3_retained", "75vx40");
        //            adjust_EventToken_Dictionary.Add("d7_retained", "laedbj");
        //            adjust_EventToken_Dictionary.Add("watch_inter_10", "xl27un");
        //            adjust_EventToken_Dictionary.Add("watch_inter_25", "fa7vmb");
        //            adjust_EventToken_Dictionary.Add("watch_rewarded_5", "df4m4b");
        //            adjust_EventToken_Dictionary.Add("watch_rewarded_15", "7wb697");
        //            adjust_EventToken_Dictionary.Add("click_reward_5", "ie7sf1");
        //            adjust_EventToken_Dictionary.Add("click_reward_15", "sjq95f");
        //            adjust_EventToken_Dictionary.Add("level_achieved_10", "he9eam");
        //            adjust_EventToken_Dictionary.Add("level_achieved_20", "g47ti1");
        //            adjust_EventToken_Dictionary.Add("completed_all_levels", "gml507");
        //#elif UNITY_IOS
        //            adjust_EventToken_Dictionary.Add("d3_retained", "ux5wg3");
        //            adjust_EventToken_Dictionary.Add("d7_retained", "m1whk7");
        //            adjust_EventToken_Dictionary.Add("watch_inter_10", "jzd0g0");
        //            adjust_EventToken_Dictionary.Add("watch_inter_25", "rx7fyc");
        //            adjust_EventToken_Dictionary.Add("watch_rewarded_5", "g5brho");
        //            adjust_EventToken_Dictionary.Add("watch_rewarded_15", "4lh4bx");
        //            adjust_EventToken_Dictionary.Add("click_reward_5", "5e3ten");
        //            adjust_EventToken_Dictionary.Add("click_reward_15", "w9pgy7");
        //            adjust_EventToken_Dictionary.Add("level_achieved_10", "g4wq0j");
        //            adjust_EventToken_Dictionary.Add("level_achieved_20", "mnz64a");
        //            adjust_EventToken_Dictionary.Add("completed_all_levels", "yyz6rd");
        //#endif
        //        }

        //        AdjustEvent adjustEvent = null;
        //        if (adjust_EventToken_Dictionary.ContainsKey(lifeTimeEventName))
        //        {
        //            string token = adjust_EventToken_Dictionary[lifeTimeEventName];
        //            adjustEvent = new AdjustEvent(token);
        //        }
        //        else
        //        {
        //            Debug.LogError("Adjust event key missing");
        //        }
        //        Adjust.trackEvent(adjustEvent);

        //        AnalyticsController.LogLifeTimeEvent(lifeTimeEventName);
        //        Debug.LogFormat("<color='red'>{0}</color>", lifeTimeEventName);
        //    }
        //    public static void LogLevelCompleted(int level)
        //    {

        //        Log(string.Format("Completed======================= {0}", level));
        //        AnalyticsController.LogLevelCompleted(level);
        //    }


        //static void Log(string log)
        //{
        //    if (logToConsole)
        //        Debug.LogFormat("<color=#FFFF00>{0}</color>",log);
        //}



        //public static void LogABTesting(string abType, string abValue)
        //{
        //    Debug.Log("<color='green'>AB value choice made type:" + abType + " and ab value: " + abValue+" in Analytics assistant</color>");
        //    AnalyticsController.LogABTesting(abType, abValue);
        //}


    }

    public enum RVState
    {
        rv_click,
        rv_start,
        rv_complete,
    }
    public enum RVPlacement
    {
        currency_multiplier,
        skin_shop_unlock,
        room_shop_unlock,
        revive,
        collectible_item_unlock,
        rewarded_level_unlock,
        rewarded_item_unlock,
        rv_option_selected,
    
}
*/