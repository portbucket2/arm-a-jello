﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DragReporter))]
public class RelativeDragDetector : MonoBehaviour
{
    public float standardDistance= 100;
    public AnimationCurve curve;
    private float normalizedStandardDistance;
    DragReporter drep;
    ArmBase abase;
    Camera cam;
    Vector2 Target 
    {
        get 
        {
            Vector3 v3 = cam.WorldToScreenPoint(abase.transform.position);
            return new Vector2(v3.x, v3.y); 
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        abase = GetComponent<ArmBase>();
        drep = GetComponent<DragReporter>();
        drep.onDrag += OnDrag;
        drep.onNoDrag += OnNoDrag;
        cam = Camera.main;
        normalizedStandardDistance = standardDistance*Screen.height/1800.0f;
    }

    // Update is called once per frame
    void OnDrag(Drag drag)
    {
        //Vector2 prevDir = (drag.GetPrevPos() - Target).normalized;
        //Vector2 currentDir = (drag.GetCurrentPos() - Target).normalized;
        //float angle = Vector2.SignedAngle(prevDir,currentDir);
        //Debug.Log(angle);

        //abase.AddRot(angle);

        Vector2 targetOriginVec = drag.GetDragOrigin() - Target;
        Vector2 perpendicularVec = Vector2.Perpendicular(targetOriginVec).normalized;
        Vector2 dragVec = drag.GetTotalDrag();

        float dot = Vector2.Dot(dragVec, perpendicularVec);

        float frac = Mathf.Clamp01(Mathf.Abs(dot) / normalizedStandardDistance);
        //Debug.LogFormat("df: {0} {1}", dot, frac);
        float f = curve.Evaluate(frac) * ((dot < 0) ? -1 : 1);
        //Debug.LogFormat("f and ndot: {0} {1}", f, frac);

        abase.SetRotMult(f);
    }
    void OnNoDrag()
    {
        abase.SetRotMult(0);
    }
}
