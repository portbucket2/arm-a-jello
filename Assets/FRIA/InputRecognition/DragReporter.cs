﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragReporter : MonoBehaviour
{
    const float defHeight = 1800;
    public float defPixCount = 15;
    public float angleBreak_degrees = 0;
    public float idleTimeLimit = 3;

    public static event System.Action<bool> onIdleFlagChanged;
    
    public event System.Action<Drag> onDrag;
    public event System.Action onNoDrag;
    public event System.Action<Vector2> onDrag_Total;
    public event System.Action<Vector2> onDrag_Incremental;
    public event System.Action<Vector3> onTap;
    float minimalPixelCount;

    public static DragReporter instance;
    public Drag currentDrag = null;
    private void Start()
    {
        idleFlag = true;
        idleTime = 3;
        instance = this;
        minimalPixelCount =  (defPixCount/ defHeight) * Screen.height;
    }

    bool idleFlag;
    float idleTime; 
    private void Update()
    {
        bool noInput = false;
        if (Input.GetMouseButtonDown(0))
        {
            currentDrag = new Drag(new Vector2(Input.mousePosition.x, Input.mousePosition.y), minimalPixelCount, angleBreak_degrees);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (currentDrag != null && currentDrag.didntDragFarEnough)
            {
                onTap?.Invoke(Input.mousePosition);
            }
            currentDrag = null;
        }
        else if (!Input.GetMouseButton(0))
        {
            noInput = true;
        }

        if (noInput)
        {
            idleTime += Time.deltaTime;
            if (idleTime >= idleTimeLimit && !idleFlag)
            {
                idleFlag = true;
                onIdleFlagChanged?.Invoke(idleFlag);
            }

        }
        else
        {
            idleTime = 0;
            if (idleFlag)
            {
                idleFlag = false;
                onIdleFlagChanged?.Invoke(idleFlag);
            }
        }
        if (currentDrag != null)
        {
            Vector2 currrentPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            Drag restartedDrag=null;

            if (currentDrag.WillCauseAngleBreak(currrentPos))
            {
                restartedDrag = new Drag(currrentPos, minimalPixelCount, angleBreak_degrees);
                //Debug.LogError("angleBreak");
            }

            currentDrag.UpdateDragCurrentPos(currrentPos);
            Vector2 vt = currentDrag.GetTotalDrag();
            Vector2 vi = currentDrag.GetIncrementalDrag();
            //Vector2 resMult = new Vector2(900.0f/Screen.width , 1800.0f/Screen.height );
            //Debug.Log(v*resMult);
            onDrag_Total?.Invoke(vt);
            onDrag_Incremental?.Invoke(vi);
            onDrag?.Invoke(currentDrag);

            if (restartedDrag != null) currentDrag = restartedDrag;
        }
        else
        {
            onNoDrag?.Invoke();
        }
    }
}

public class Drag
{
    bool angleBreakEnabled { get { return angleBreak_degrees > 0; } }
    float angleBreak_degrees;
    Vector2 dragOrigin;
    Vector2 prevPos;
    Vector2 latestPos;
    bool dragConfirmed;
    public Vector2 travelVec { get { return latestPos - dragOrigin; } }
    //float startTime;
    float minPixel = 100;

    public bool didntDragFarEnough
    {
        get
        {
            return travelVec.magnitude < minPixel;
        }
    }
    public Drag(Vector2 startPosition, float minPix, float angleBreak_degrees)
    {
        this.angleBreak_degrees = angleBreak_degrees;
        this.minPixel = minPix;
        //startTime = Time.time;
        dragOrigin = startPosition;
        prevPos = startPosition;
        latestPos = startPosition;
        dragConfirmed = false;
    }

    public bool WillCauseAngleBreak(Vector2 currentPosition)
    {
        if (!angleBreakEnabled) return false;

        Vector2 ORG_LAST = latestPos - dragOrigin;
        Vector2 LAST_CURRENT = currentPosition - latestPos;
        if (LAST_CURRENT.magnitude < minPixel) return false;
        float angle = Vector2.Angle(ORG_LAST,LAST_CURRENT);
        //Debug.LogFormat("angle: {0}", angle);
        return angle > angleBreak_degrees;
    }

    public void UpdateDragCurrentPos(Vector2 currentPosition)
    {
        prevPos = latestPos;
        latestPos = currentPosition;
    }
    public Vector2 GetTotalDrag()
    {
        if (!dragConfirmed && didntDragFarEnough)
        {
            return Vector2.zero;
        }
        else
        {
            dragConfirmed = true;
            return latestPos-dragOrigin;
        }
    }
    public Vector2 GetIncrementalDrag()
    {
        if (!dragConfirmed && didntDragFarEnough)
        {
            return Vector2.zero;
        }
        else
        {
            dragConfirmed = true;
            return latestPos-prevPos;
        }
    }
    public Vector2 GetDragOrigin()
    {
        return dragOrigin;
    }

    public Vector2 GetCurrentPos()
    {
        return latestPos;
    }
    public Vector2 GetPrevPos()
    {
        return prevPos;
    }
}
